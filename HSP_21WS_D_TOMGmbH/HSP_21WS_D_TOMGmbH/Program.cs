﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sprint1
{
    class Program
    {

        static void Main()
        {

            double l, h, S, H, B, masse, SW;
            int g;
            string m;

            Console.WriteLine("Willkommen bei TOMGmbH");

            Console.WriteLine("Bitte Geben Sie die Länge in mm an.");
            l = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine();

            Console.WriteLine("Bitte Geben Sie die Gewindelänge in mm an.");
            h = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine();




            Console.WriteLine("Bitte Geben Sie die Gewinde(Durchmesser) an.");
            g = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine();

            Console.WriteLine("Bitte Geben Sie die Material an.");
            m = Convert.ToString(Console.ReadLine());

            Console.WriteLine();


            //Steigung
            S = h / 0.613;
            Console.WriteLine("Steigung  in mm = " + S);
            Console.WriteLine();

            //Höhe
            H = 0.866 * S;
            Console.WriteLine("Höhe  in mm = " + H);
            Console.WriteLine();


            //Breite
            B = 0.366 * S - 0.54 * SpitzenSpiel(S);
            Console.WriteLine("Breite  in mm = " + B);
            Console.WriteLine();

            //Komplette Geometrie
            string GeometrieFormat;
            GeometrieFormat = String.Format("{0}{1}{2}*{3}", g, l, B, H);
            Console.WriteLine(GeometrieFormat);
            Console.WriteLine();

            //Volumen

            Console.WriteLine("Volumen  in mm3 = " + Volumen(S, g));
            Console.WriteLine();

            //Dichte Probe
            /*
            Console.WriteLine("Dichte  in g/mm3 = " + Dichte(m));
            Console.WriteLine();*/

            //masse
            masse = Volumen(S, g) * Dichte(m);
            Console.WriteLine("Masse  in g = " + masse);
            Console.WriteLine();

            //Preis     
            Console.WriteLine("Preis in Euro = " + Preis(m));
            Console.WriteLine();

            //Schraubenkopbreite     
            Console.WriteLine("Schraubkopfbreite in mm " + g);
            Console.WriteLine();

            //Schlüsselweite     
            SW = Math.Sqrt((g * g) - (l * l));
            Console.WriteLine("Schlüsselweite in mm " + SW);
            Console.WriteLine();

            static double SpitzenSpiel(double S)
            {
                if (S == 1.5) return 0.15;
                else if ((S >= 2) & (S <= 5)) return 0.25;
                else if ((S >= 6) & (S <= 12)) return 0.5;
                else if ((S >= 14) & (S <= 44)) return 1;
                else return 0;
            }
            static double Volumen(double S, double g) //mm3
            {
                double V, r1, r2;
                if (S == 1.5) { r1 = 0.075; r2 = 0.15; V = 2 * S * ((r1 - r2) * (r1 - r2)) * g; return V; }
                else if ((S >= 2) & (S <= 5)) { r1 = 0.125; r2 = 0.25; V = 2 * S * ((r1 - r2) * (r1 - r2)) * g; return V; }
                else if ((S >= 6) & (S <= 12)) { r1 = 0.25; r2 = 0.5; V = 2 * S * ((r1 - r2) * (r1 - r2)) * g; return V; }
                else if ((S >= 14) & (S <= 44)) { r1 = 0.5; r2 = 1; V = 2 * S * ((r1 - r2) * (r1 - r2)) * g; return V; }
                else return 0;
            }

            static double Dichte(string m) // g/mm3
            {
                switch (m)
                {
                    case "stahl":
                    case "Stahl":
                    case "STAHL": return 0.007850;

                    case "Edelstahl":
                    case "edelstahl":
                    case "EDELSTAHL": return 0.00085;

                    case "Aluminium":
                    case "aluminium":
                    case "ALUMiNIUM": return 0.00027;

                    case "Kupfer":
                    case "kupfer":
                    case "KUPFER":
                        return 0.000896;
                        break;
                }
                return 0;


            }
            static double Preis(string m) //Euro
            {
                switch (m)
                {
                    case "stahl":
                    case "Stahl":
                    case "STAHL": return 0.32;

                    case "Edelstahl":
                    case "edelstahl":
                    case "EDELSTAHL": return 0.12;

                    case "Aluminium":
                    case "aluminium":
                    case "ALUMiNIUM": return 2.03;

                    case "Kupfer":
                    case "kupfer":
                    case "KUPFER":
                        return 0.17;
                        break;
                }
                return 0;


            }

        }
    }
}